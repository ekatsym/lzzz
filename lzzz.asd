(defsystem "lzzz"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("alexandria"
               "optima")
  :components ((:module "src"
                :components
                ((:file "core")
                 (:file "pattern")
                 (:file "package"))))
  :description ""
  :in-order-to ((test-op (test-op "lzzz/tests"))))

(defsystem "lzzz/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("lzzz"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for lzzz"
  :perform (test-op (op c) (symbol-call :rove :run c)))
