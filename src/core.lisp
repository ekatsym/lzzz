(defpackage lzzz.core
  (:use :cl)
  (:export #:promise
           #:promise?
           #:delay
           #:force))
(in-package :lzzz.core)


(defstruct (promise (:constructor make-promise (thunk))
                    (:predicate promise?)
                    (:print-object (lambda (o s)
                                     (if (promise-forced? o)
                                         (format s "#<PROMISE-forced cache: ~s>"
                                                 (promise-cache o))
                                         (format s "#<PROMISE-unforced>")))))
  (thunk thunk)
  (forced? nil)
  (cache nil))

(defmacro delay (&body body)
  `(make-promise (lambda () ,@body)))

(defun force (promise)
  (if (promise? promise)
      (with-slots (thunk forced? cache) promise
        (unless forced?
          (setf forced? t
                cache (funcall thunk)))
        cache)
      (error 'type-error
             :datum promise
             :expected-type 'promise)))
