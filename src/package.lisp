(defpackage lzzz
  (:use :cl
        :lzzz.core
        :lzzz.pattern)
  (:export #:promise
           #:promise?
           #:delay
           #:force))
