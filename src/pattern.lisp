(defpackage lzzz.pattern
  (:use :cl
        :lzzz.core
        :optima.core))
(in-package :lzzz.pattern)


(defstruct (promise-pattern (:include constructor-pattern)
                            (:constructor make-promise-pattern
                             (thunk-pattern &aux (subpatterns (list thunk-pattern)))))
  (thunk-pattern thunk-pattern))

(defmethod constructor-pattern-destructor-sharable-p ((x promise-pattern) (y promise-pattern))
  t)

(defmethod constructor-pattern-make-destructor ((pattern promise-pattern) var)
  (make-destructor :predicate-form `(promise? ,var)
                   :accessor-forms `((force ,var))))

(defmethod parse-constructor-pattern ((name (eql 'delay)) &rest args)
  (apply #'make-promise-pattern (mapcar #'parse-pattern args)))

(defmethod unparse-pattern ((pattern promise-pattern))
  `(delay ,(unparse-pattern (promise-pattern-thunk-pattern pattern))))
