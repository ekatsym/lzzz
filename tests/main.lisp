(defpackage lzzz/tests/main
  (:use :cl
        :lzzz
        :rove))
(in-package :lzzz/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :lzzz)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
